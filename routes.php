<?php

require_once 'controllers/SubscriberController.php';
require_once 'controllers/SpeakerController.php';
require_once 'controllers/MeetupController.php';


use Pecee\SimpleRouter\SimpleRouter;

// on ajoute un préfixe car le site se trouve dans mon cas à l'adresse : 
// http://localhost/simplon/routeur/ 
//$prefix = '/simplon/Projects/API'; 
$prefix = '/MeetupProject/'; 
 
SimpleRouter::group(['prefix' => $prefix], function() { 
SimpleRouter::get('/', 'DefaultController@defaultAction'); 
SimpleRouter::get('/login', 'LoginController@error')->name('login');

SimpleRouter::get('/allSubscribers', 'SubscriberController@getSubscriber'); 
SimpleRouter::post('/subscribers', 'SubscriberController@insertSubscriber'); 
SimpleRouter::post('/subscribers/{id}', 'SubscriberController@getWithId');
SimpleRouter::delete('/subscribers/{id}', 'SubscriberController@deleteSubscriber');

SimpleRouter::get('/allSpeakers', 'SpeakerController@getSpeaker');
SimpleRouter::post('/speakers', 'SpeakerController@insertSpeaker');
SimpleRouter::post('/speakers/{id}', 'SpeakerController@getWithId');
SimpleRouter::delete('/speakers/{id}', 'SpeakerController@deleteSpeaker');

SimpleRouter::get('/allMeetup', 'MeetupController@getMeetup');
SimpleRouter::post('/meetup', 'MeetupController@insertMeetup');
SimpleRouter::delete('/meetup/{id}', 'MeetupController@deleteMeetup');
SimpleRouter::put('/meetup/{id}', 'MeetupController@updateMeetup');
SimpleRouter::get('/meetup/{id}', 'MeetupController@getMeetupById');
});

?>
