<?php

require_once 'vendor/autoload.php';

use Pecee\SimpleRouter\SimpleRouter;
require_once 'routes.php';

// Start the routing
SimpleRouter::start();

?>