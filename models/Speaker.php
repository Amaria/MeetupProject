<?php

include_once 'config.php'; 

class Speaker {
    private $id;
    private $firstName;
    private $lastName;
    private $description;
   


    public function getSpeaker(){
          
        $bdd = connect(); 
        $reponse = $bdd->query('SELECT * FROM speaker'); 
        $speaker = $reponse->fetchAll(); 
        return $speaker;


    }

    public function deleteSpeaker($id){
          
        $bdd = connect(); 
        $request = $bdd->prepare('DELETE FROM speaker WHERE id=:id'); 
        $request->execute(['id'=>$id]); 

    }

    public function insertSpeaker($first_name,$last_name,$description){
            $bdd = connect(); 
            
            $done = $bdd->prepare('INSERT INTO speaker (first_name, last_name, description) VALUES (:first_name, :last_name, :description)'); 
            $done->execute(array( 
                    'first_name'=>$first_name, 
                    'last_name'=>$last_name, 
                    'description'=>$description
                )); 

                $data = [
                    'id' => $bdd->lastInsertId(),
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'description' => $description
                ];

                return $data;  

        }

   
}

?>