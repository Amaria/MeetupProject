<?php

include_once 'config.php'; 

class Subscriber {
    private $firstName;
    private $lastName;
    private $mailAddr;
    private $dateSubscription;

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function getMailAddr() {
        return $this->mailAddr;
    }

    public function setMailAddr($mailAddr) {
        $this->mailAddr = $mailAddr;
    }

    public function getDateSubscription() {
        return $this->dateSubscription;
    }

    public function setDateSubscription($dateSubscription) {
        $this->dateSubscription = $dateSubscription;
    }
  

    public function getSubscriber() {
        // Ici votre requête SQL (peut-être qu'un extends serait sympa pour la connexion en DB...)
        // Les données ci-dessous son fakes !

        // return [
        //     ['first_name' => 'Jack', 'last_name' => 'Dawson', 'mail_addr' => 'jack@dawson.com', 'date_subscription' => '2018-02-25 12:15:00'],
        //     ['first_name' => 'Bob', 'last_name' => 'Dylan', 'mail_addr' => 'bob@dylan.com', 'date_subscription' => '2018-02-25 12:15:00'],
        //     ['first_name' => 'Gérard', 'last_name' => 'Depardieu', 'mail_addr' => 'gerard@depardieu.com', 'date_subscription' => '2018-02-25 12:15:00']
        // ];

        $bdd = connect(); 
        $reponse = $bdd->query('SELECT * FROM subscriber'); 
        $subscriber = $reponse->fetchAll(); 
        return $subscriber; 
    }

    
    public function deleteSubscriber($id){
          
        $bdd = connect(); 
        $request = $bdd->prepare('DELETE FROM subscriber WHERE id=:id'); 
        $request->execute(['id'=>$id]); 
        return $request;

    }



    public function insertSubscriber($first_name,$last_name,$mail_addr,$date){ 
        $bdd = connect(); 
        
        $done = $bdd->prepare('INSERT INTO subscriber (first_name, last_name, mail_addr /*,date*/) VALUES (:first_name, :last_name, :mail_addr/*, :date*/)'); 
        $done->execute(array( 
                'first_name'=>$first_name, 
                'last_name'=>$last_name, 
                'mail_addr'=>$mail_addr,
                // 'date'=>$date
            )); 
        
            $data = [
                'id' => $bdd->lastInsertId(),
                'first_name' => $first_name,
                'last_name'=>$last_name, 
                'mail_addr' => $mail_addr,
                'date_subscription'=>$date
            ];
    
            return $data; 

        // if ($done){ 
            // echo '<h3>'.'Vous vous êtes inscris avec succès!'.'</h3>'; 
            // header('refresh:2;URL=../'); 
            //  } 
    } 


}
