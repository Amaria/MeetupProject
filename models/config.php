<?php 
 
   function connect() { 
    $user = 'root'; 
    $password = 'histoires'; 
    $host = 'localhost'; 
    $database = 'meetup_project'; 
    $bdd = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password); 
    // active les exceptions PDO 
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
    return $bdd; 
 } 
 
?>