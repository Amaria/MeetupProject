<?php

include_once 'config.php'; 

class Meetup {

    private $title;
    private $description;
    private $image;
    private $date;
    private $location_id;
    private $subscriber_id;
    private $speaker_id;
    private $id;


    public function getMeetup(){
          
        $bdd = connect(); 
        $reponse = $bdd->query('SELECT meetup.*, location.lieu FROM `meetup` INNER JOIN location ON meetup.location_id=location.id'); 
        
        return $reponse->fetchAll(); 

    }


    public function getMeetupById($id){
          
        $bdd = connect(); 
        $reponse = $bdd->query('SELECT meetup.*, location.id as location_id, location.lieu FROM `meetup` INNER JOIN location ON meetup.location_id=location.id WHERE meetup.id = ' . $id); 
        return $reponse->fetch(); 

    }

    public function deleteMeetup($id){
          
        $bdd = connect(); 
        $request = $bdd->prepare('DELETE FROM meetup WHERE id=:id'); 
        $request->execute(['id'=>$id]); 


    }

    public function insertMeetup($title,$description,$image,$date,$lieu){ 
        $bdd = connect(); 
        
        $location = $bdd->prepare('INSERT INTO location (lieu) VALUES (:lieu)');
        $location->execute(array(
            'lieu'=>$lieu
        ));


        $done = $bdd->prepare('INSERT INTO meetup (title, description, image, date, location_id) VALUES (:title, :description,:image,:date, (SELECT MAX(`id`) FROM location))'); 
        $done->execute(array( 
                'title'=>$title, 
                'description'=>$description, 
                'image'=>$image,
                'date'=>$date
            ));

        $data = [
            'id' => $bdd->lastInsertId(),
            'title' => $title,
            'description' => $description,
            'image'=>$image,
            'date' => $date,
            'lieu' => $lieu
        ];

        return $data;     
    } 


    public function updateMeetup($id,$title,$description,$image,$date,$location_id,$lieu){ 
        $bdd = connect(); 
        


        $done = $bdd->prepare('UPDATE meetup SET title=:title, description=:description, image=:image, date=:date WHERE id=:id'); 
        $done->execute(array( 
                'id'=>$id,
                'title'=>$title, 
                'description'=>$description, 
                'image'=>$image,
                'date'=>$date
            ));

        $location = $bdd->prepare('UPDATE location SET lieu=:lieu WHERE id=:location_id') ;
        $location->execute(array(
            'location_id' => $location_id,
            'lieu'=>$lieu
            
        ));


            $data = [
                'id' => $id,
                'title' => $title,
                'description' => $description,
                'image'=>$image,
                'date' => $date,
                'lieu' => $lieu
            ];
    
            return $data; 

        }

}

?>