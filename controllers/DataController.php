<?php

// Récupère les données envoyées
// quelque soit la méthode HTTP : GET/POST/PUT/PATCH/DELETE

    function getHttpData() {
        // GET
        if ($_SERVER['REQUEST_METHOD'] == 'GET') return $_GET;
        // POST
        if ($_SERVER['REQUEST_METHOD'] == 'POST') return $_POST;
        // PUT / PATCH / DELETE
        parse_str(file_get_contents("php://input"),$data);
        return $data;
    }

   
    //
    //On recupere nos data à la place de mettre $_POST[''] on met $['email'];

?>