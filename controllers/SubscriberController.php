<?php

require_once 'controllers/Controller.php';
require_once 'models/Subscriber.php';

class SubscriberController extends Controller
    {

    function getSubscriber() {
        $subscriber = new Subscriber();
        return json_encode($subscriber->getSubscriber());
    }


    function deleteSubscriber($id){
        $request = new Subscriber();
        return json_encode($request->deleteSubscriber($id));
    }

    function insertSubscriber(){
        $prenom = $_POST['first_name'];
        $nom = $_POST['last_name'];
        $mail = $_POST['mail_addr'];
        $date = date("Y-m-d H:i:s");
        $subscriber = new Subscriber();
        $response = $subscriber->insertSubscriber($prenom,$nom,$mail,$date);

        return json_encode($response);
    }


    function getWithId($id) {
        return json_encode('coucou'.$id);
    }


}

?>