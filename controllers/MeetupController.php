<?php

require_once 'Controller.php';
require_once './models/Meetup.php';
require_once 'DataController.php';

class MeetupController extends Controller {
    function getMeetup() {
        // $token = $this->getToken();
        $meetup = new Meetup();
        return json_encode($meetup->getMeetup());
    }

    function getMeetupById($id) {
        // $token = $this->getToken();
        $meetup = new Meetup();
        return json_encode($meetup->getMeetupById($id));
    }

    function deleteMeetup($id) {
        $request = new Meetup();
        return json_encode($request->deleteMeetup($id));
    }

    function insertMeetup(){
        $title = $_POST['title'];
        $description = $_POST['description'];
        $image = $_POST['image'];
        $date = $_POST['date'];
        $lieu = $_POST['lieu'];
        
        $meetup = new Meetup();
        $response = $meetup->insertMeetup($title,$description,$image,$date,$lieu);

        return json_encode($response);
    }

 
    function updateMeetup($id){
        $data = getHTTPData();
        // $data = $data['token'];
        
        $title = $data['title'];
        $description = $data['description'];
        $image = $data['image'];
        $date = $data['date'];
        $lieu = $data['lieu'];
        $location_id = $data['location_id'];

        $meetup = new Meetup();
        $response = $meetup->updateMeetup($id, $title,$description,$image,$date,$location_id,$lieu);

        return json_encode($response);
    }
   


    function getWithId($id) {
        return json_encode('coucou'.$id);
    }

}

?>