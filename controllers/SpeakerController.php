<?php

require_once 'controllers/Controller.php';
require_once 'models/Speaker.php';

class SpeakerController {

    function getSpeaker() {
        $speaker = new Speaker();
        return json_encode($speaker->getSpeaker());
    }


    function deleteSpeaker($id){
        $request = new Speaker();
        return json_encode($request->deleteSpeaker($id));
    }

    function insertSpeaker(){
        $prenom = $_POST['first_name'];
        $nom = $_POST['last_name'];
        $description = $_POST['description'];
        
        $speaker = new Speaker();
        $response = $speaker->insertSpeaker($prenom,$nom,$description);

        return json_encode($response);
    }

    function getWithId($id) {
        return json_encode('coucou'.$id);
    }



}

?>