
//Affichage des formulaires

$('.buttonMeetups').click(function () {
    $('.meetupsForm').animate({ height: "toggle", opacity: "toggle" }, "slow")

});

$('.buttonSpeakers').click(function () {
    $('.speakersForm').animate({ height: "toggle", opacity: "toggle" }, "slow")
});

$('.buttonSubscribers').click(function () {
    $('.subscribersForm').animate({ height: "toggle", opacity: "toggle" }, "slow")
});


/*document.querySelector('.Tester').addEventListener('click', function(){
    alert('ça marche');
    document.querySelector('.edition').style.display="block";

});*/


/*document.querySelector('.Tester').addEventListener('mouseover',function(){alert('coucou')});*/
//--------------Suppression du Meetup 


function deleteMeetup(id) {

    $.ajax({
        url: "meetup/" + id,
        method: 'DELETE'
    }).done(function (msg) {
       //console.log('delete done', msg)
        $(`[data-idmeetup=${id}]`).remove()
    })

}

function deleteSpeaker(id) {

    $.ajax({
        url: "speakers/" + id,
        method: 'DELETE'
    }).done(function (msg) {
        //console.log('delete done', msg)
        $(`[data-idSpeaker=${id}]`).remove()
    })

}

function deleteSubscriber(id) {

    $.ajax({
        url: "subscribers/" + id,
        method: 'DELETE'
    }).done(function (msg) {
        console.log('delete done', msg)
        //console.log('delete done', msg)
        $(`[data-idSubscriber=${id}]`).remove()
    })

}

//Edition des meetups sur popup classe="edition"

function edit(id) {
    $.ajax({
        url: "meetup/" + id,
        method: "GET"
    }).done(function (msg) {
        var data = JSON.parse(msg);
        $('.idEdit').val(data['id']);
        $('.titleEdit').val(data['title']);
        $('.descriptionEdit').val(data['description']);
        $('.imgEdit').val(data['image']);
        $('.dateEdit').val(data['date']);
        $('.lieuEdit').val(data['lieu']);
        $('.locationidEdit').val(data['location_id']);
    });
    $('.edition').css({
        position: 'absolute',
        width: $(document).width(),
        height: $(document).height()
    });
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $('.edition').animate({ height: "toggle", opacity: "toggle" }, "slow");
    
    $('.close').click(function(){
        console.log('.close')
        $('.edition').hide( "slow" );
    });

}


//Insertions des données Meetups ,Speakers ,Suscribers
//Affichage des données Meetups ,Speakers ,Suscribers 

window.onload = function () {

    //--------Insertion des Meetups ça Marche 

    $('.meetupsForm').submit(function (event) {
        event.preventDefault();

        var title = document.querySelector('.titleAdd').value
        var description = document.querySelector('.descriptionAdd').value
        var image = document.querySelector('.imgAdd').value
        var date = document.querySelector('.dateAdd').value
        var lieu = document.querySelector('.lieuAdd').value
        var location_id = document.querySelector('.locationidAdd').value
        $.ajax({
            url: "meetup",
            method: 'POST',
            data: { title: title, description: description, image: image, date: date, lieu: lieu, location_id: location_id }
        }).done(function (response) {
            json = JSON.parse(response);
            // mettre a jour le DOM
            $('.meetup').append(` 
                <div class="" data-idmeetup="${json.id}">

                <h3>${json.title}</h3>  
                <h3>${json.description}</h3>
                <div class="img" style="background-image:url(${json.image})";>
                </div>
                <h3>${json.date}</h3>
                <h3>${json.lieu}</h3>
                
                <button class="edit" onclick="edit(${json.id});">Editer</button>
                <button class="trash" onclick="deleteMeetup(${json.id})">Supprimer</button><hr>
                </div>`);
        });
    })

    //---------------------Update des Meetups

    $('.form-edition').submit(function (event) {
        event.preventDefault();
        var id = document.querySelector('.idEdit').value
        var title = document.querySelector('.titleEdit').value
        var description = document.querySelector('.descriptionEdit').value
        var image = document.querySelector('.imgEdit').value
        var date = document.querySelector('.dateEdit').value
        var lieu = document.querySelector('.lieuEdit').value
        var location_id = document.querySelector('.locationidEdit').value
        $.ajax({
            url: "meetup/"+id,
            method: 'PUT',
            data: {id: id, title: title, description: description, image: image, date: date, lieu: lieu, location_id: location_id }
        }).done(function (response) {
            json = JSON.parse(response);
            // mettre a jour le DOM
            $(`[data-idmeetup=${json.id}]`).html(` 

                <div class="" data-idmeetup="${json.id}">
                <h3>${json.title}</h3>  
                <h3>${json.description}</h3>
                <div class="img" style="background-image:url(${json.image})";>
                </div>
                <h3>${json.date}</h3>
                <h3>${json.lieu}</h3>
               
                <button class="edit" onclick="edit(${json.id});">Editer</button>
                <button class="trash" onclick="deleteMeetup(${json.id})">Supprimer</button><hr>
                </div>`);

        });
    })



    //-----------------Affichage des Meetups ça Marche

    $.ajax({
        url: "allMeetup",
        method: "GET"
    }).done(function (msg) {
        json = JSON.parse(msg);
        json.forEach(function (element) {
            $('.meetup').append(` 
        
            <div class="" data-idmeetup="${element['id']}">

                
                <h3>${element['title']}</h3>  
                <h3>${element['description']}</h3>  
                <div class="img" style="background-image:url(${element['image']});">
                </div>
                <h3>${element['date']}</h3>
                <h3>${element['lieu']}</h3>
                

                <iframe width="295" height="150" frameborder="0" style="border:0"
               src="https://www.google.com/maps/embed/v1/place?q=${element['lieu']}&key=AIzaSyD6M54yHAP5HWdkDgDa1UBgTNsAflARzvE" allowfullscreen></iframe><hr>

                <button class="edit" onclick="edit(${element['id']});">Editer</button>
                <button class="trash" onclick="deleteMeetup(${element['id']})">Supprimer</button>
        
            </div><hr>`);

        });
    })


    //------------------Insertion des intervenants ça Marche

    $('.speakersForm').submit(function (event) {
        event.preventDefault();
        var first_name = document.querySelector('.nomSpeakers').value;
        var last_name = document.querySelector('.prenomSpeakers').value;
        var description = document.querySelector('.descriptSpeakers').value;
        // var date= document.querySelector('.dateSubscribers').value;
        $.ajax({
            url: "speakers",
            method: 'POST',
            data: {first_name: first_name, last_name: last_name, description: description }
        }).done(function (response) {
            json = JSON.parse(response);
            $('.speaker').append(` 
            <div class="" data-idSpeaker="${json.id}">
            <h3>${json.first_name}</h3>  
            <h3>${json.last_name}</h3>
            <h3>${json.description}</h3>
            <button onclick="deleteSpeaker(${json.id})">Supprimer le speaker</button><hr>
            </div>`);
        });

    })


    //------------Affichage des intervenants/speakers ça Marche

    $.ajax({
        url: "allSpeakers",
        method: "GET"
    }).done(function (msg) {
        json = JSON.parse(msg)
        json.forEach(function (element) {
            $('.speaker').append(`
            <div class="" data-idSpeaker="${element['id']}">
                <h3>${element['first_name']}</h3>
                <h3>${element['last_name']}</h3>
                <h3>${element['description']}</h3><hr>
                <button onclick="deleteSpeaker(${element['id']})">Supprimer le speaker</button><hr>
                </div>`);
        });

    });



    //-----------Insertion des subscribers ça marche pas

    $('.subscribersForm').submit(function (event) {
        event.preventDefault();
        var first_name = document.querySelector('.nomSubscribers').value;
        var last_name = document.querySelector('.prenomSubscribers').value;
        var mail_addr = document.querySelector('.mailSubscribers').value;
        // var date= document.querySelector('.dateSubscribers').value;
        $.ajax({
            url: "subscribers",
            method: 'POST',
            data: { first_name: first_name, last_name: last_name, mail_addr: mail_addr }
        }).done(function (response) {
            json = JSON.parse(response);
            $('.subscriber').append(` 
                <div class="" data-idSubscriber="${json.id}">
                <h3>${json.first_name}</h3>  
                <h3>${json.last_name}</h3>
                <h3>${json.mail_addr}</h3>
                <h3>${json.date_subscription}</h3>
                <button onclick="deleteSubscriber(${json.id})">Supprimer les inscrits</button><hr>
                </div>`);
        });

    })



    //Affichage des inscrits (subscribers) 

    $.ajax({

        url: "allSubscribers",
        method: "GET"
    }).done(function (msg) {
        json = JSON.parse(msg);
        json.forEach(function (element) {

            $('.subscriber').append(`
            <div class="" data-idSubscriber="${element['id']}">
                <h3>${element['first_name']}</h3>
                <h3>${element['last_name']}</h3>
                <h3>${element['mail_addr']}</h3>
                <h3>${element['date_subscription']}</h3><hr>
                <button onclick="deleteSubscriber(${element['id']})">Supprimer les inscrits</button><hr>
                </div>`);

        });

    });

    


}

